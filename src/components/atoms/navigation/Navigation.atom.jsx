import React from 'react';
import { Nav, } from 'react-bootstrap';
import { Link, NavLink } from 'react-router-dom';

export const Navigation = (props) => {

    return (
        <>
            <Nav.Item className={"navigation-links " + props.className}>
                <NavLink to={props.to}>{props.content}</NavLink>
            </Nav.Item>
        </>
    );
}
