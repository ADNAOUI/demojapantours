import React from 'react';

import { Navigation } from '../../atoms';
import { Navbar, Container, Offcanvas, Nav } from 'react-bootstrap'

export const LeftNavBar = (props) => {

  return (
    <>
      {/* <aside className='left-bar'>
        <Navigation
          content="Home"
          to="/"
        />
        <Navigation
          content="Explorer"
          to="/"
        />
        <Navigation
          content="Mangas"
          to="/"
        />
        <Navigation
          content="Animes"
          to="/"
        />
      </aside> */}

      <Navbar bg="light" expand="sm" className="mb-3">
        <Container fluid>
          <Navbar.Brand href="#">Navbar Offcanvas</Navbar.Brand>
          <Navbar.Toggle aria-controls={`offcanvasNavbar-expand-sm`} />
          <Navbar.Offcanvas
            aria-labelledby={`offcanvasNavbarLabel-expand-sm`}
            placement="start"
          >
            <Offcanvas.Header closeButton>
              <Offcanvas.Title id={`offcanvasNavbarLabel-expand-sm`}>
                Offcanvas
              </Offcanvas.Title>
            </Offcanvas.Header>
            <Offcanvas.Body>
              <Nav className="justify-content-end flex-grow-1 pe-3">
                <Nav.Link href="#action1">Home</Nav.Link>
                <Nav.Link href="#action2">Link</Nav.Link>

              </Nav>
            </Offcanvas.Body>
          </Navbar.Offcanvas>
        </Container>
      </Navbar>
    </>
  );
}
