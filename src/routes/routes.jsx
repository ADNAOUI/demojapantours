import React, { useEffect, useState } from 'react';
import { useRoutes, useLocation } from "react-router"


//Import des composants
import { Header, Footer, LeftNavBar } from "../components/organismes";

const ScrollToTop = (props) => {
    const location = useLocation();
    useEffect(() => {
        window.scrollTo(0, 0);
    }, [location]);

    return <>{props.children}</>
};

const RouteAsObj = () => {
    const routes = useRoutes([
        // {
        //     path: "/",
        //     element: <Home />,
        // },
        {
            // path: "contact",
            // element: <Contact />,
            // children can be used to configure nested routes
            // children: [
            //   { path: "child1", element: <Home /> },
            //   { path: "child2", element: <Footer /> },
            // ],
        },
        // {
        //     path: "/aPropos",
        //     element: <Apropos />,
        // },
        // {
        //     path: "/menu",
        //     element: <MenusList />,
        // },
        // {
        //     path: "/promo",
        //     element: <PromosList />,
        // },
        // {
        //     path: "/restaurants",
        //     children: [
        //         { path: "map", element: <DisplayRestaurants /> },
        //         { path: ":idRestaurant", element: <DisplayRestaurantChild /> },
        //     ],
        // },
        // {
        //     path: "/*",
        //     element: <Error />,
        // },
    ]);



    return (
        <>
            <ScrollToTop>
                <Header />
                <LeftNavBar />
                <main id="main" className="">
                    {routes}
                </main>
                <Footer />
            </ScrollToTop>
        </>
    )
}

export default RouteAsObj