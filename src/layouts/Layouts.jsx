import * as React from "react";

import {Routes, Route, Navigate } from "react-router-dom";
import RouteAsObj from '../routes/routes.jsx'

const Layouts = () => {
  return (
    <>
      <Routes>
        <Route path="/good_food/*" element={<RouteAsObj />} />
        <Route path="/" element={<Navigate replace to="/good_food/" />} />
      </Routes>
    </>
  );
}
export default Layouts;
